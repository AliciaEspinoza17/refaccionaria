import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-componente1',
  templateUrl: './componente1.component.html',
  styleUrls: ['./componente1.component.css']
})
export class Componente1Component {
  objeto1= {
    nombre : "Porcayo Espinoza Alicia",
    carrera : 'Sistemas Computacionales',
    semestre : '8bo',
    pasatiempo : 'Viajar',
    colorfav : 'Negro y Azul',
  } 
  constructor() {
    
  
  }
 
}
