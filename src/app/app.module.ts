import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { Componente1Component } from './component/componente1/componente1.component';
import { Componenete2Component } from './component/componenete2/componenete2.component';

@NgModule({
  declarations: [
    AppComponent,
    Componente1Component,
    Componenete2Component
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
